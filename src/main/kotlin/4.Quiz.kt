import java.util.*

abstract class Question (val title:String, val answer:String) {
    var status = false
    fun check(response:String){
        status = response.lowercase()==answer.lowercase()
    }
}

class FreeTextQuestion(title: String, answer: String):Question(title, answer)
class MultipleChoiceQuestion(title: String, answer: String, val options:List<String>):Question(title, answer)
class Quiz(val questions: List<Question>)

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)

    val freeText = FreeTextQuestion("Quina és la capital de França?", "Paris")
    val multChoice = MultipleChoiceQuestion("Quina és la capital d'Espanya?", "Madrid", listOf("Moscú", "Lisboa", "Madrid", "Canada"))
    val quiz = Quiz(listOf(freeText,multChoice))
    
    for (question in quiz.questions) {
        println(question.title)
        println("Resposta:")
        question.check(scanner.next())
    }
    for (question in quiz.questions) println(question.status)
}