class Estudiant(val nom:String, var nota:Nota)

enum class Nota{
    SUSPES,
    APROVAT,
    BE,
    NOTABLE,
    EXCELLENT
}
fun main(){
    val estudiant1 = Estudiant("Margaux", Nota.EXCELLENT)
    val estudiant2 = Estudiant("Jordi", Nota.BE)
    println(estudiant1)
    println(estudiant2)
}