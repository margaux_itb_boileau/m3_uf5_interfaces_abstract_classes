import java.util.*

enum class RainbowColors {
    RED, ORANGE, YELLOW,
}

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val input = scanner.next().uppercase()
    println(input in RainbowColors.values().toString())
}