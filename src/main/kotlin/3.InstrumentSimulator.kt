

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}
abstract class Instrument(){
    abstract fun makeSounds(n:Int)
}
class Triangle(val resonancia:Int):Instrument(){
    override fun makeSounds(n:Int){
        for (i in 1..n) {
            print("T")
            for (j in 1..resonancia) print("i")
            println("NC")
        }

    }
}
class Drump(val to:String):Instrument(){
    override fun makeSounds(n:Int){
        for (i in 1..n) {
            when (to) {
                "A" -> println("TAAAM")
                "O" -> println("TOOOM")
                "U"-> println("TUUM")

            }
        }
    }
}